<?php

/**
 * Define the internationalization functionality
 *
 * Loads and defines the internationalization files for this plugin
 * so that it is ready for translation.
 *
 * @link       http:\\creativelycloudy.com
 * @since      1.0.0
 *
 * @package    Ccbb_Html_Emmet
 * @subpackage Ccbb_Html_Emmet/includes
 */

/**
 * Define the internationalization functionality.
 *
 * Loads and defines the internationalization files for this plugin
 * so that it is ready for translation.
 *
 * @since      1.0.0
 * @package    Ccbb_Html_Emmet
 * @subpackage Ccbb_Html_Emmet/includes
 * @author     Jeff Chown <jchown@successtech.com>
 */
class Ccbb_Html_Emmet_i18n {


	/**
	 * Load the plugin text domain for translation.
	 *
	 * @since    1.0.0
	 */
	public function load_plugin_textdomain() {

		load_plugin_textdomain(
			'ccbb-html-emmet',
			false,
			dirname( dirname( plugin_basename( __FILE__ ) ) ) . '/languages/'
		);

	}



}
