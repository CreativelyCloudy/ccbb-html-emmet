<?php

/**
 * Fired during plugin activation
 *
 * @link       http:\\creativelycloudy.com
 * @since      1.0.0
 *
 * @package    Ccbb_Html_Emmet
 * @subpackage Ccbb_Html_Emmet/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Ccbb_Html_Emmet
 * @subpackage Ccbb_Html_Emmet/includes
 * @author     Jeff Chown <jchown@successtech.com>
 */
class Ccbb_Html_Emmet_Activator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function activate() {

	}

}
