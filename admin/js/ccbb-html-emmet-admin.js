(function( $ ) {
	'use strict';

	/**
	 * All of the code for your admin-facing JavaScript source
	 * should reside in this file.
	 *
	 * Note: It has been assumed you will write jQuery code here, so the
	 * $ function reference has been prepared for usage within the scope
	 * of this function.
	 *
	 * This enables you to define handlers, for when the DOM is ready:
	 *
	 * $(function() {
	 *
	 * });
	 *
	 * When the window is loaded:
	 *
	 * $( window ).load(function() {
	 *
	 * });
	 *
	 * ...and/or other possibilities.
	 *
	 * Ideally, it is not considered best practise to attach more than a
	 * single DOM-ready or window-load handler for a particular page.
	 * Although scripts in the WordPress core, Plugins and Themes may be
	 * practising this, we should strive to set a better example in our own work.
	 */
	$(function() {

		// enable Emmet capability for Ace editors
		// *only* if ace global is defined
		if ( typeof ace !== 'undefined' ) {
			ace.require('ace/ext/emmet');
			// when BB Theme active, enable Emmet in:
			// - Code | CSS in Customizer
			ace.edit( $('#customize-control-fl-css-code .ace_editor')[0] ).setOption( 'enableEmmet', true );
			// - Code | Head Code in Customizer
			ace.edit( $('#customize-control-fl-head-code .ace_editor')[0] ).setOption( 'enableEmmet', true );
			// - Code | Header Code in Customizer
			ace.edit( $('#customize-control-fl-header-code .ace_editor')[0] ).setOption( 'enableEmmet', true );
			// - Code | Footer Code in Customizer
			ace.edit( $('#customize-control-fl-footer-code .ace_editor')[0] ).setOption( 'enableEmmet', true );
		}

	});


})( jQuery );
