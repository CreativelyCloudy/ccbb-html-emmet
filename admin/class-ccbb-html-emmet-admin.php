<?php

/**
 * The admin-specific functionality of the plugin.
 *
 * @link       http:\\creativelycloudy.com
 * @since      1.0.0
 *
 * @package    Ccbb_Html_Emmet
 * @subpackage Ccbb_Html_Emmet/admin
 */

/**
 * The admin-specific functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Ccbb_Html_Emmet
 * @subpackage Ccbb_Html_Emmet/admin
 * @author     Jeff Chown <jchown@successtech.com>
 */
class Ccbb_Html_Emmet_Admin {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of this plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;

	}

	/**
	 * Register the stylesheets for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Ccbb_Html_Emmet_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Ccbb_Html_Emmet_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/ccbb-html-emmet-admin.css', array(), $this->version, 'all' );

	}

	/**
	 * Register the JavaScript for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Ccbb_Html_Emmet_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Ccbb_Html_Emmet_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/ccbb-html-emmet-admin.js', array( 'jquery' ), $this->version, false );

		// Add Emmet functionality to WP Customizer 'Code' editors
		// IF FLCustomizer class exists (indicating BB Theme is active)
		if ( class_exists( 'FLCustomizer' ) ) {
			wp_enqueue_script( 'ccbb-html-emmet-core', CCBB_HTML_EMMET_URL . 'lib/js/emmet.js', array(), $this->version, true );
			wp_enqueue_script( 'ace-emmet', FL_THEME_URL . '/js/ace/ext-emmet.js', array('ace'), FL_THEME_VERSION, true);
		}
	}

}
