<?php

/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @link              http:\\creativelycloudy.com
 * @since             1.0.0
 * @package           Ccbb_Html_Emmet
 *
 * @wordpress-plugin
 * Plugin Name:       CCBB Html Emmet
 * Description:       This plugin adds Emmet to BeaverBuilder's Html Module and to WP's Customizer Code menu when BB Theme is active
 * Version:           1.1.1
 * Author:            Jeff Chown
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       ccbb-html-emmet
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

define('CCBB_HTML_EMMET_FILE', __FILE__);
define('CCBB_HTML_EMMET_DIR', plugin_dir_path(CCBB_HTML_EMMET_FILE));
define('CCBB_HTML_EMMET_URL', plugins_url('/', CCBB_HTML_EMMET_FILE));


/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-ccbb-html-emmet-activator.php
 */
function activate_ccbb_html_emmet() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-ccbb-html-emmet-activator.php';
	Ccbb_Html_Emmet_Activator::activate();
}

/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/class-ccbb-html-emmet-deactivator.php
 */
function deactivate_ccbb_html_emmet() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-ccbb-html-emmet-deactivator.php';
	Ccbb_Html_Emmet_Deactivator::deactivate();
}

register_activation_hook( __FILE__, 'activate_ccbb_html_emmet' );
register_deactivation_hook( __FILE__, 'deactivate_ccbb_html_emmet' );

/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path( __FILE__ ) . 'includes/class-ccbb-html-emmet.php';

/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    1.0.0
 */
function run_ccbb_html_emmet() {

	$plugin = new Ccbb_Html_Emmet();
	$plugin->run();

}
run_ccbb_html_emmet();
