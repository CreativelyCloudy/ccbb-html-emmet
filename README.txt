=== Plugin Name ===
Contributors: Jeff Chown
Tags: beaver, builder, html, module, emmet
Requires at least: 4.4
Tested up to: 4.4.2
Stable tag: 1.1.1
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

This plugin adds Emmet capability to the HTML module in the Beaver Builder 
plugin (required).

== Description ==

This plugin adds Emmet capability to the HTML module (which uses Ace Editor) 
in the Beaver Builder plugin (required).  It has been tested with 
Wordpress 4.4+ and Beaver Builder 1.7+.

If there is enough interest, I plan to extend this plugin to enable further
customization of the Ace Editor within BB's HTML module.

== Installation ==

1. Download ccbb-html-emmet.zip from this repo (in root folder)
2. In your Wordpress site choose the Plugins | Add New menu
3. Using the Upload Plugin button, upload ccbb-html-emmet.zip

== Frequently Asked Questions ==

== Changelog ==

= 1.0 =
* initial commit

= 1.1.0 =

- Emmet now active in WP Customizer | Code | CSS, Head Code, Header Code, 
  Footer Code editors when BB Theme is active
- can now be used with BB Plugin and/or BB Theme

= 1.1.1 = 
* fixed bug - Add Media button was causing js error and not displaying 
  Media window - caused by duplicate definition of Underscore library
  (in emmet.js); as of v3.5 Wordpress automatically includes Underscore library
